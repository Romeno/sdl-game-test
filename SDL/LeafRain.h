#pragma once
#include "Singleton.h"
#include "Defines.h"

#ifdef PLATFORMER_GAME_TYPE

struct Leaf
{
	SDL_Point pos;

	signed char oscilateDir;
	float angleX;
	int stratX;
	int type;
	float angleSelf;
	float startSelfAngle;
	int onBackground;
	int groundY;
};


class LeafRain : public Singleton<LeafRain>
{
public:
	LeafRain();
	~LeafRain();

	int Init();
	void Tick(Uint32 diff);
	void Render(bool foreground);
	void Clean();

	int LoadLeafs();

private:
	SDL_Texture *leafTypes[4];
	std::vector<Leaf> leafs;

	int leafCount;
	int leafSpeed;
	int leafAmplitude = 10;
};

#endif