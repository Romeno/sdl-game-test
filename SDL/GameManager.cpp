#include "stdafx.h"
#include "Defines.h"
#include "GameManager.h"
#include "Utils.h"
#include "SystemInfo.h"
#include "CoordinateSystem.h"
#include "LevelInfo.h"
#include "Logger.h"
#include "WindowTitleManager.h"


#ifdef PLATFORMER_GAME_TYPE
#include "PlatformerIH.h"
#include "PlatformerPC.h"
#include "PlatformerSC.h"
#include "LeafRain.h"
#endif


#ifdef POINT_SPLASH_GAME_TYPE
#include "PointIH.h"
#include "PointSplashPC.h"
#include "PointSplashSC.h"
#endif


#ifdef LINE_CASCADE_GAME_TYPE
#include "LineIH.h"
#include "LineCascadePC.h"
#include "PointSplashSC.h"
#endif


GameManager::GameManager()
	: vpHeight(480)
	, vpWidth(640)

	, win(nullptr)
	, ren(nullptr)

	, winX(0)
	, winY(0)

	, m_fps(0)

	, m_pc(nullptr)
	, m_ih(nullptr)
	, m_sc(nullptr)
{

}


GameManager::~GameManager()
{
	delete m_ih;
	delete m_pc;
	delete m_sc;
}


int GameManager::Init()
{
	int ret;
	ret = InitLogger();
	if (ret != 0)
	{
		ERR(ERR_TYPE_ENGINE_ERROR, L"InitLogger failed");
		_getch();
		return ret;
	}

	ret = InitSDL();
	if (ret != 0) 
	{
		ERR(ERR_TYPE_ENGINE_ERROR, L"InitSDL failed");
		SDL_StopTextInput();
		SDL_Quit();
		_getch();
		return ret;
	}

	ret = InitEngine();
	if (ret != 0)
	{
		ERR(ERR_TYPE_ENGINE_ERROR, L"InitEngine failed");
		cleanup(win, ren);
		SDL_StopTextInput();
		SDL_Quit();
		_getch();
		return ret;
	}

	PrintInfo(win, ren);

	return 0;
}


int GameManager::InitLogger()
{
	return ::InitLogger();
}


int GameManager::InitSDL() 
{
	if (SDL_Init(SDL_INIT_EVERYTHING)) {
		ERR(ERR_TYPE_SDL_ERROR, L"SDL_Init error: %s", SDL_GetError());
		return 1;
	}

	win = SDL_CreateWindow("Leaf-rain Walk!", -1, -1, vpWidth, vpHeight, SDL_WINDOW_SHOWN);
	if (win == nullptr)
	{
		ERR(ERR_TYPE_SDL_ERROR, L"SDL_CreateWindow error: %s", SDL_GetError());
		return 1;
	}

	CenterWindow();

	int numRenderDirever = -1;
	ren = SDL_CreateRenderer(win, numRenderDirever, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) {
		ERR(ERR_TYPE_SDL_ERROR, L"SDL_CreateRenderer error: %s", SDL_GetError());
		cleanup(win);
		return 1;
	}

	SDL_StartTextInput();

	return 0;
}


void GameManager::CenterWindow()
{
	SDL_DisplayMode mode;
	int display = SDL_GetWindowDisplayIndex(win);
	SDL_GetDesktopDisplayMode(display, &mode);

	winX = (mode.w - vpWidth) / 2;
	winY = (mode.h - vpHeight) / 2;

	SDL_SetWindowPosition(win, winX, winY);
}


int GameManager::InitEngine()
{
	int ret;

#ifdef PLATFORMER_GAME_TYPE
	m_ih = new PlatformerIH();
	m_pc = new PlatformerPC();
	m_sc = new PlatformerSC();
#endif

#ifdef LINE_CASCADE_GAME_TYPE
	m_ih = new LineIH();
	m_pc = new LineCascadePC();
	m_sc = new PointSplashSC();
#endif

#ifdef POINT_SPLASH_GAME_TYPE
	m_ih = new PointIH();
	m_pc = new PointSplashPC();
	m_sc = new PointSplashSC();
#endif

#ifdef PLATFORMER_GAME_TYPE
	m_ih = new QixIH();
	m_pc = new QixPC();
	m_sc = new QixSC();
#endif

	// LEVEL_INFO
	LevelInfo::Inst()->Init(2560, 1920);


	// INPUT_HANDLER
	m_ih->Init();


	// PLAYER_CONTROLLER
	ret = m_pc->Init();
	if (ret)
	{
		ERR(ERR_TYPE_ENGINE_ERROR, L"PlrCtrl Init failed");
		return ret;
	}

	glm::dvec3 wPos = { -vpWidth / 2, 0, 0 };
	PC()->SetWPos(wPos, PIVOT_CENTER);


	// SCREEN_CONTROLLER
	m_sc->Init(wPos);


	// OTHER_INSTANCES
#ifdef PLATFORMER_GAME_TYPE
	ret = LeafRain::Inst()->Init();
	if (ret)
	{
		PC()->Clear();
		ERR(ERR_TYPE_ENGINE_ERROR, L"LeafRain Init failed");
		return ret;
	}
#endif

	return 0;
}


void GameManager::MainLoop()
{
	static Uint32 lastTicks = SDL_GetTicks();

	SDL_Event e;

	bool quit = false;

	while (!quit) {
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				m_ih->HandleKeyDown(&e.key);

				//PrintKeyboardEvent(&e);
			}
			else if (e.type == SDL_KEYUP)
			{
				m_ih->HandleKeyUp(&e.key);

				//PrintKeyboardEvent(&e);
			}
			else if (e.type == SDL_KEYMAPCHANGED)
			{
				//PrintKeyboardEvent(&e);
			}
			else if (e.type == SDL_TEXTEDITING) //CJK
			{
				//PrintTextEditingEvent(&e);
			}
			else if (e.type == SDL_TEXTINPUT)
			{
				//PrintTextInputEvent(&e);
			}
			else if (e.type == SDL_MOUSEBUTTONDOWN)
			{
				m_ih->HandleMouseButtonDown(&e.button);

				//PrintMouseButtonEvent(&e);
			}
			else if (e.type == SDL_MOUSEBUTTONUP)
			{
				m_ih->HandleMouseButtonUp(&e.button);

				//PrintMouseButtonEvent(&e);
			}
			else if (e.type == SDL_MOUSEWHEEL)
			{
				//PrintMouseWheelEvent(&e);
			}
			else if (e.type == SDL_MOUSEMOTION)
			{
				m_ih->HandleMouseMotion(&e.motion);

				//PrintMouseMotionEvent(&e);
			}
		}

		Uint32 newTicks = SDL_GetTicks();

		Tick(newTicks - lastTicks);

		RenderScene(newTicks - lastTicks);

		lastTicks = newTicks;
	}
}


void GameManager::Tick(Uint32 diff) 
{
	WindowTitleManager::Inst()->Tick(diff);

	m_ih->Tick(diff);

	m_pc->Tick(diff);

	m_sc->Tick(diff);

#ifdef PLATFORMER_GAME_TYPE
	LeafRain::Inst()->Tick(diff);
#endif
}


void GameManager::Clean()
{
	SDL_StopTextInput();

	cleanup(ren, win);

	SDL_Quit();
}


void GameManager::RenderScene(Uint32 diff)
{
	//Render the scene
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 0);
	SDL_RenderClear(ren);

	OutputFPS();

	//SDL_Point imagePos = { (vpWidth - imageLength) / 2, (vpHeight - imageHeight) / 2 };

#ifdef PLATFORMER_GAME_TYPE
	LeafRain::Inst()->Render(true);
#endif

	m_pc->Render();

#ifdef PLATFORMER_GAME_TYPE
	LeafRain::Inst()->Render(false);
#endif

	//renderTexture(image, renderer, x, y);

	SDL_RenderPresent(ren);
}


//void GameManager::DrawPoints()
//{
//	SDL_SetRenderDrawColor(ren, 255, 255, 255, 0);
//
//	EnterCriticalSection(&addPointCS);
//
//	for (int i = 0; i < ps.size(); ++i) {
//		SDL_RenderDrawPoint(ren, W2Sx(ps[i].x), W2Sy(ps[i].y));
//	}
//
//	LeaveCriticalSection(&addPointCS);
//}
//
//
//void GameManager::DrawLines()
//{
//	SDL_SetRenderDrawColor(ren, 255, 255, 255, 0);
//	EnterCriticalSection(&addPointCS);
//
//	std::vector<SDL_Point> pps;
//
//	SDL_RenderDrawLines(ren, pStart.data(), pStart.size());
//	SDL_RenderDrawLines(ren, pEnd.data(), pEnd.size());
//
//	//for (int i = 0; i < pStart.size(); ++i) {
//	//SDL_RenderDrawLines(ren, pStart.data(), pStart.size());
//	//SDL_RenderDrawLines(ren, pEnd.data(), pStart.size());
//	//}
//
//	LeaveCriticalSection(&addPointCS);
//}


//void GameManager::ClearPoints()
//{
//	EnterCriticalSection(&addPointCS);
//
//	ps.clear();
//
//	primitivecount = 0;
//
//	LeaveCriticalSection(&addPointCS);
//}
//
//
//void GameManager::ClearLines()
//{
//	EnterCriticalSection(&addPointCS);
//
//	pStart.clear();
//	pEnd.clear();
//
//	primitivecount = 0;
//
//	LeaveCriticalSection(&addPointCS);
//}


void GameManager::OutputFPS() {
	static int frameCount = 0;
	static std::chrono::high_resolution_clock::time_point lastTimePoint = std::chrono::high_resolution_clock::now();
	static std::chrono::high_resolution_clock::time_point nowTimePoint;

	++frameCount;
	nowTimePoint = std::chrono::high_resolution_clock::now();
	long long diff = nowTimePoint.time_since_epoch().count() - lastTimePoint.time_since_epoch().count();
	if (diff > 1000000000) // every 1 second (10000000000 in nanoseconds)
	{
		m_fps = (float)frameCount / ((float)diff / (float)1000000000);
		lastTimePoint = lastTimePoint + std::chrono::seconds(1);
		frameCount = 0;

		std::string title = "Draw FPS: ";
		title += std::to_string(m_fps);

		WindowTitleManager::Inst()->SetWindowTitle(title);
	}
}


#ifdef PLATFORMER_GAME_TYPE
PlatformerPC* PC()
{
	return (PlatformerPC*)GameManager::Inst()->GetPC();
}


PlatformerIH* IH()
{
	return (PlatformerIH*)GameManager::Inst()->GetIH();
}


PlatformerSC* SC()
{
	return (PlatformerSC*)GameManager::Inst()->GetSC();
}
#endif


#ifdef LINE_CASCADE_GAME_TYPE
LineCascadePC* PC() 
{
	return (LineCascadePC*)GameManager::Inst()->GetPC();
}


LineIH* IH()
{
	return (LineIH*)GameManager::Inst()->GetIH();
}


PointSplashSC* SC()
{
	return (PointSplashSC*)GameManager::Inst()->GetSC();
}
#endif


#ifdef POINT_SPLASH_GAME_TYPE
PointSplashPC* PC()
{
	return (PointSplashPC*)GameManager::Inst()->GetPC();
}


PointIH* IH()
{
	return (PointIH*)GameManager::Inst()->GetIH();
}


PointSplashSC* SC()
{
	return (PointSplashSC*)GameManager::Inst()->GetSC();
}

#endif


/*
 *
 * Romeno's code
 * To be Romeno Company
 * Project: Learning game development
 *
 */