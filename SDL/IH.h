#pragma once

#ifdef PLATFORMER_GAME_TYPE
#include "PlatformerIH.h"
#endif


#ifdef POINT_SPLASH_GAME_TYPE
#include "PointIH.h"
#endif


#ifdef LINE_CASCADE_GAME_TYPE
#include "LineIH.h"
#endif


#ifdef QIX_GAME_TYPE
#include "QixIH.h"
#endif
