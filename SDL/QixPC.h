#pragma once
#include "PlayerController.h"

class QixPC : public PlayerController
{
public:
	QixPC();
	virtual ~QixPC();

	virtual int Init();
	virtual void Render();
	virtual void Tick(Uint32 diff);
	virtual void Clear();

	glm::dvec3		m_wPos;
};

