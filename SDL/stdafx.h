// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define NOMINMAX

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_Image.h>
#include <conio.h>
#include <random>
#include <math.h>
#include <chrono>

#include <windows.h>
#include <glm/glm.hpp>

// TODO: reference additional headers your program requires here
