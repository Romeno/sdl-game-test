#include "stdafx.h"
#include "Defines.h"

#ifdef PLATFORMER_GAME_TYPE

#include "LeafRain.h"
#include "Utils.h"
#include "SC.h"


LeafRain::LeafRain()
{

}


LeafRain::~LeafRain()
{

}


int LeafRain::Init()
{
	return LoadLeafs();
}


int LeafRain::LoadLeafs()
{
	std::string baseImagePath = GetResourcePath() + "Leaf_0";
	std::string imagePath = "";

	for (size_t i = 0; i < 4; i++)
	{
		imagePath = baseImagePath + std::to_string(i + 1) + ".png";
		SDL_Surface *img = IMG_Load(imagePath.c_str());
		if (img == nullptr) {
			return 1;
		}

		leafTypes[i] = SDL_CreateTextureFromSurface(ren, img);
		SDL_FreeSurface(img);
		if (tex == nullptr) {
			return 1;
		}
	}

	leafSpeed = 1;

	return 0;
}


void LeafRain::Tick(Uint32 diff)
{
	SDL_Point newp;
	Leaf l;
	static int counter = 0;
	counter++;

	if (counter > 15) {
		counter = 0;

		for (size_t i = 0; i < (int)Random(0, 5); i++)
		{
			int startX = (int)Random(0, vpWidth);
			newp = { startX, -20 };
			l = {
				newp,
				Random(-1, 1) < 0 ? -1 : 1,
				0,
				startX,
				(int)round(Random(0.5, 3.5)),
				0,
				Random(30, 50),
				(int)round(Random(-0.5, 1.5)),
				(int)Random(vpHeight / 2.0f - 10, vpHeight / 2.0f + 20)
			};
			leafs.push_back(l);
		}
	}

	for (size_t i = 0; i < leafs.size(); i++)
	{
		if (leafs[i].pos.y < leafs[i].groundY)
		{
			leafs[i].angleX += M_PI / 180.0;
			if (leafs[i].angleX > 2 * M_PI)
			{
				leafs[i].angleX -= 2 * M_PI;
			}

			leafs[i].pos.y += leafSpeed;
			leafs[i].pos.x = leafs[i].stratX + leafs[i].oscilateDir * (float)leafAmplitude * sin(leafs[i].angleX);
			leafs[i].angleSelf = leafs[i].startSelfAngle + 30.0f * sin(leafs[i].angleX);
		}
	}
}


void LeafRain::Render(bool foreground)
{
	//int leafCount = Random(30, 50);

	glm::dvec3 scrnWPos = SC()->GetWPos();

	SDL_Rect dstrect1;
	for (size_t i = 0; i < leafs.size(); i++)
	{
		if (leafs[i].onBackground != foreground)
		{
			dstrect1 = {
				leafs[i].pos.x,
				leafs[i].pos.y,
				30,
				30
			};
			SDL_RenderCopyEx(ren, leafTypes[leafs[i].type], NULL, &dstrect1, leafs[i].angleSelf, NULL, SDL_FLIP_NONE);
		}
	}

	//std::vector<Leaf> newLeafs;
	//for (size_t i = 0; i < leafs.size(); i++)
	//{
	//	if (leafs[i].pos.y < vpHeight)
	//	{
	//		newLeafs.push_back(leafs[i]);
	//	}
	//}

	//leafs.clear();
	//for (size_t i = 0; i < newLeafs.size(); i++)
	//{
	//	leafs.push_back(newLeafs[i]);
	//}
}


void LeafRain::Clean()
{

}


#endif