#pragma once

#ifdef PLATFORMER_GAME_TYPE
#include "PlatformerPC.h"
#endif


#ifdef POINT_SPLASH_GAME_TYPE
#include "PointSplashPC.h"
#endif


#ifdef LINE_CASCADE_GAME_TYPE
#include "LineCascadePC.h"
#endif


#ifdef QIX_GAME_TYPE
#include "QixPC.h"
#endif
